package com.msr.data;

import com.msr.model.SiteUse;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SiteUseRepository extends JpaRepository<SiteUse, Integer> {

}
