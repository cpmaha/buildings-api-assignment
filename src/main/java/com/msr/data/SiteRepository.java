package com.msr.data;

import com.msr.model.Site;
import com.msr.model.SiteUse;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * A sample JPA repository for querying and storing sites
 */
public interface SiteRepository extends PagingAndSortingRepository<Site, Integer> {

}