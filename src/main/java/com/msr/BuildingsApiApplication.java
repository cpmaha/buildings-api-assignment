package com.msr;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.msr.model.Site;
import com.msr.model.SiteUse;
import com.msr.model.UseType;
import com.msr.util.SiteService;
import com.msr.util.SiteUseService;
import com.msr.util.UseTypeService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@SpringBootApplication
public class BuildingsApiApplication {

	public static void main(String[] args) {

		SpringApplication.run(BuildingsApiApplication.class, args);
	}

	@Bean
	CommandLineRunner runner(SiteService siteService, UseTypeService useTypeService, SiteUseService siteUseService){
		return args -> {
//			 read JSON and load json
			ObjectMapper mapper = new ObjectMapper();
			TypeReference<List<Site>> typeReference = new TypeReference<List<Site>>(){};
			InputStream inputStream = TypeReference.class.getResourceAsStream("/data/sites.json");
			try {
				List<Site> sites = mapper.readValue(inputStream,typeReference);
				siteService.save(sites);
				System.out.println("Sites Saved!");

			} catch (IOException e){
				System.out.println("Unable to save sites: " + e.getMessage());
			}

			// load user types
			mapper = new ObjectMapper();
			TypeReference<List<UseType>> useTypetypeReference = new TypeReference<List<UseType>>(){};
			inputStream = TypeReference.class.getResourceAsStream("/data/use_types.json");
			try {
				List<UseType> useTypes = mapper.readValue(inputStream,useTypetypeReference);
				useTypeService.save(useTypes);
				System.out.println("useTypes Saved!");

			} catch (IOException e){
				System.out.println("Unable to save useTypes: " + e.getMessage());
			}

			//load site uses
			mapper = new ObjectMapper();
			TypeReference<List<SiteUse>> siteUsetypeReference = new TypeReference<List<SiteUse>>(){};
			inputStream = TypeReference.class.getResourceAsStream("/data/site_uses.json");
			mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);

			try {
				List<SiteUse> siteUses = mapper.readValue(inputStream,siteUsetypeReference);
				siteUseService.save(siteUses);
				System.out.println("siteUses Saved!");
			} catch (IOException e){
				System.out.println("Unable to save siteUses: " + e.getMessage());
			}
		};
	}
}
