package com.msr.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.IntNode;
import com.fasterxml.jackson.databind.node.LongNode;

import java.io.IOException;

public class SiteUseDeserializer extends StdDeserializer<SiteUse> {

    public SiteUseDeserializer() {
        this(null);
    }

    public SiteUseDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public SiteUse deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException {
        JsonNode node = jp.getCodec().readTree(jp);
        int id = (Integer) node.get("id").numberValue();
        String description = node.get("description").asText();
        int siteId = (Integer) node.get("site_id").numberValue();
        int useTypeId = (Integer) node.get("use_type_id").numberValue();
        long sizeSqft = node.get("size_sqft").longValue();

        return new SiteUse(id, new UseType(useTypeId),
                 new Site(siteId), description, sizeSqft);

    }
}