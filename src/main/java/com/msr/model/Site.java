package com.msr.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@EqualsAndHashCode
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Site {
    @Id
    private int id;

    private String name;

    private String address;

    private String city;

    private String state;

    private String zipcode;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "siteId",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<SiteUse> siteUses = new HashSet<>();

    private long totalSize;

    @Transient
    private UseType primaryType;



    public Site(int siteId) {
        this.id = siteId;
    }
}
