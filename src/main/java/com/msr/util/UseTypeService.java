package com.msr.util;

import com.msr.data.UseTypeRepository;
import com.msr.model.UseType;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UseTypeService {

    private UseTypeRepository useTypeRepository;

    public UseTypeService(UseTypeRepository useTypeRepository){
        this.useTypeRepository = useTypeRepository;
    }

    public Iterable<UseType> save(List<UseType> useTypes){
        return useTypeRepository.saveAll(useTypes);
    }
}
