package com.msr.util;

import com.msr.data.SiteUseRepository;
import com.msr.model.SiteUse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SiteUseService {

    private SiteUseRepository siteUseRepository;

    public SiteUseService(SiteUseRepository siteUseRepository){
        this.siteUseRepository = siteUseRepository;
    }

    public Iterable<SiteUse> save(List<SiteUse> siteUses){
        return siteUseRepository.saveAll(siteUses);
    }
}
