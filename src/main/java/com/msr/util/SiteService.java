package com.msr.util;

import com.msr.data.SiteRepository;
import com.msr.model.Site;
import com.msr.model.SiteUse;
import com.msr.model.UseType;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class SiteService {

    public SiteRepository siteRepository;


    public SiteService(SiteRepository siteRepository){
        this.siteRepository = siteRepository;
    }

    public Iterable<Site> list() {
        Iterable<Site> siteList = siteRepository.findAll();

        for (Site site :
                siteList) {
            extractTotalSizeAndPrimaryUsage(site);
        }

        return siteList;
    }

    public Optional<Site> findById(int id){
        Site site = siteRepository.findById(id).get();
//        System.out.println("---- site ----" + site.toString());
        extractTotalSizeAndPrimaryUsage(site);

        return Optional.of(site);
    }

    private void extractTotalSizeAndPrimaryUsage(Site site) {
        Set<SiteUse> siteUses = site.getSiteUses();
        long totalSize = 0;
        HashMap<UseType, Long> usageType = new HashMap<>();

        for (SiteUse siteUse :
                siteUses) {
            totalSize += siteUse.getSizeSqft();
            System.out.println(siteUse);
            UseType key = siteUse.getUseTypeId();
            if(usageType.containsKey(key)) {
                Long newUsageType = usageType.get(key) + siteUse.getSizeSqft();
                usageType.put(key, newUsageType);
            } else {
                usageType.put(key, siteUse.getSizeSqft());
            }

//            System.out.println("----usageType----" + usageType);
        }

        Map.Entry<UseType, Long> maxEntry = null;

        for (Map.Entry<UseType, Long> entry :
                usageType.entrySet()) {
            if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0)
            {
                maxEntry = entry;
            }
        }

//        System.out.println("------maxEntry.getKey()-----" + maxEntry.getKey());
        site.setPrimaryType(maxEntry.getKey());
        site.setTotalSize(totalSize);
    }

    public Site save(Site site){
        return siteRepository.save(site);
    }

    public Iterable<Site> save(List<Site> sites){
       return siteRepository.saveAll(sites);
    }
}
